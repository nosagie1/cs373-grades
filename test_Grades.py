#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = line-too-long
# pylint: disable = missing-docstring

# --------------
# test_Grades.py
# --------------

# -------
# imports
# -------

import unittest  # main, TestCase

import Grades

# ----------------
# test_grades_eval
# ----------------


class test_grades_eval(unittest.TestCase):
    def test_0(self) -> None:
        l_l_scores: list[list[int]] = [
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
            ],
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "F")

    def test_1(self) -> None:
        l_l_scores: list[list[int]] = [
            [1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
            ],
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "F")

    def test_2(self) -> None:
        l_l_scores: list[list[int]] = [
            [2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
            [
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
                2,
            ],
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "A")

    def test_3(self) -> None:
        l_l_scores: list[list[int]] = [
            [3, 3, 3, 3, 3],
            [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3],
            [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3],
            [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3],
            [
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
                3,
            ],
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "A")


class test_letter_grade(unittest.TestCase):
    def test_0(self) -> None:
        grade: int = 0
        letter: str = Grades.letter_grade(grade)
        self.assertEqual(letter, "F")

    def test_1(self) -> None:
        grade: int = 5
        letter: str = Grades.letter_grade(grade)
        self.assertEqual(letter, "C")

    def test_2(self) -> None:
        grade: int = 11
        letter: str = Grades.letter_grade(grade)
        self.assertEqual(letter, "A")

    def test_3(self) -> None:
        grade: int = 9
        letter: str = Grades.letter_grade(grade)
        self.assertEqual(letter, "B+")

    def test_4(self) -> None:
        grade: int = 1
        letter: str = Grades.letter_grade(grade)
        self.assertEqual(letter, "D-")

    def test_5(self) -> None:
        grade: int = 10
        letter: str = Grades.letter_grade(grade)
        self.assertEqual(letter, "A-")


class test_leter_calculator(unittest.TestCase):
    def test_0(self) -> None:
        category: int = 0
        pass_count: int = 2
        grade: int = Grades.letter_calculator(category, pass_count)
        self.assertEqual(grade, 0)

    def test_1(self) -> None:
        category: int = 2
        pass_count: int = 11
        grade: int = Grades.letter_calculator(category, pass_count)
        self.assertEqual(grade, 7)

    def test_2(self) -> None:
        category: int = 3
        pass_count: int = 12
        grade: int = Grades.letter_calculator(category, pass_count)
        self.assertEqual(grade, 9)

    def test_3(self) -> None:
        category: int = 4
        pass_count: int = 40
        grade: int = Grades.letter_calculator(category, pass_count)
        self.assertEqual(grade, 11)

    def test_4(self) -> None:
        category: int = 4
        pass_count: int = 25
        grade: int = Grades.letter_calculator(category, pass_count)
        self.assertEqual(grade, 1)

    def test_5(self) -> None:
        category: int = 0
        pass_count: int = 3
        grade: int = Grades.letter_calculator(category, pass_count)
        self.assertEqual(grade, 3)


class test_grade_counter(unittest.TestCase):
    def test_0(self) -> None:
        scores: list[int] = [0, 0, 0, 0, 0]
        count: int = Grades.grade_counter(scores)
        self.assertEqual(count, 0)

    def test_1(self) -> None:
        scores: list[int] = [1, 1, 1, 1, 1]
        count: int = Grades.grade_counter(scores)
        self.assertEqual(count, 0)

    def test_2(self) -> None:
        scores: list[int] = [1, 1, 1, 3, 3]
        count: int = Grades.grade_counter(scores)
        self.assertEqual(count, 3)

    def test_3(self) -> None:
        scores: list[int] = [2, 2, 2, 2, 2]
        count: int = Grades.grade_counter(scores)
        self.assertEqual(count, 5)

    def test_4(self) -> None:
        scores: list[int] = [3, 3, 3, 3, 3]
        count: int = Grades.grade_counter(scores)
        self.assertEqual(count, 5)

    def test_5(self) -> None:
        scores: list[int] = [3, 1, 1, 1, 3, 3]
        count: int = Grades.grade_counter(scores)
        self.assertEqual(count, 4)


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    unittest.main()
