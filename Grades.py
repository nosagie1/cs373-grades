#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = line-too-long

# ---------
# Grades.py
# ---------

# https://docs.python.org/3.6/library/functools.html
# https://sphinx-rtd-tutorial.readthedocs.io/en/latest/docstrings.html

# -----------
# grades_eval
# -----------

MAX_SCORE = 11


def grades_eval(l_l_scores: list[list[int]]) -> str:
    assert l_l_scores
    min_grade = MAX_SCORE  # highest score possible is A (11)
    category = 0

    for scores in l_l_scores:  # calculate final letter grade
        pass_count = grade_counter(scores)
        min_grade = min(
            letter_calculator(category, pass_count), min_grade
        )  # use lowest grade of all categories
        category += 1

    return letter_grade(min_grade)


def grade_counter(
    scores: list[int],
) -> int:  # count number of passing grades (2s & 3s + adjustments)
    assert scores

    one_count = 0
    three_count = 0
    pass_count = 0

    for score in scores:
        if score == 1:
            one_count += 1
        if score == 3:
            three_count += 1
        if score >= 2:
            pass_count += 1

    while one_count > 0 and three_count >= 2:  # adjust 1s from pairs of 3s
        one_count -= 1
        three_count -= 2
        pass_count += 1

    return pass_count


def letter_calculator(category: int, pass_count: int) -> int:
    assert 0 <= category <= 4
    assert 0 <= pass_count <= 42

    grade_map = (
        {  # mapping of number representing letter grades from number of passing grades
            0: {5: 11, 4: 9, 3: 3},
            1: {12: 11, 11: 11, 10: 9, 9: 6, 8: 4, 7: 1},
            2: {14: 11, 13: 11, 12: 9, 11: 7, 10: 5, 9: 3, 8: 1},
            3: {14: 11, 13: 11, 12: 9, 11: 7, 10: 5, 9: 3, 8: 1},
            4: {
                42: 11,
                41: 11,
                40: 11,
                39: 11,
                38: 10,
                37: 9,
                36: 8,
                35: 8,
                34: 7,
                33: 6,
                32: 6,
                31: 5,
                30: 4,
                29: 4,
                28: 3,
                27: 2,
                26: 1,
                25: 1,
            },
        }
    )

    if (
        category in grade_map and pass_count in grade_map[category]
    ):  # return grade if not 0
        return grade_map[category][pass_count]
    return 0


def letter_grade(num_grade: int) -> str:
    assert 0 <= num_grade <= 11

    letter_map = {
        11: "A",
        10: "A-",
        9: "B+",
        8: "B",
        7: "B-",
        6: "C+",
        5: "C",
        4: "C-",
        3: "D+",
        2: "D",
        1: "D-",
        0: "F",
    }

    return letter_map[num_grade]
